package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Customer;
import com.bwc.app.consentform.model.CustomerAgree;
import com.bwc.app.consentform.model.Template;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerAgreeRepository extends JpaRepository<CustomerAgree, Long> {

    List<CustomerAgree> findByCustomer(Customer customer);

    CustomerAgree findById(Long id);

    CustomerAgree findByCustomerAndTemplate(Customer customer, Template template);

}
