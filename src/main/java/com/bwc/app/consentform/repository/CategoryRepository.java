package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findById(Long id);

    Category findByAlias(String alias);

    Page<Category> findAllByRolesIn(Role role, Pageable pageable);

}
