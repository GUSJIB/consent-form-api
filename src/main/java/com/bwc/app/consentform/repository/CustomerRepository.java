package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findByHn(String hn);

    Page<Customer> findAllByHn(String hn, Pageable pageable);

    @Query(value = "select c from #{#entityName} c where lower(c.hn) like %?1% or lower(c.firstnameTh) like %?1% or lower(c.middlenameTh) like %?1% or lower(c.lastnameTh) like %?1% or lower(c.firstnameEn) like %?1% or lower(c.middlenameEn) like %?1% or lower(c.lastnameEn) like %?1%")
    Page<Customer> search(String query, Pageable pageable);

}
