package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Attachment;
import com.bwc.app.consentform.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

    Attachment findByFileName(String fileName);

    Attachment findByCustomerAndId(Customer customer, Long id);

    List<Attachment> findBydocumentId(Long documentId);
}
