package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Template;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TemplateRepository extends JpaRepository<Template, Long> {

    Template findById(Long id);

    Template findByName(String name);

    List<Template> findByCategory(Category category);

    Page<Template> findAll(Pageable pageable);

}
