package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByAlias(String alias);

    Role findByName(String name);

}
