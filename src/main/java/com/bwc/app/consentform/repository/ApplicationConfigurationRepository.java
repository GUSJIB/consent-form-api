package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.ApplicationConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationConfigurationRepository extends JpaRepository<ApplicationConfiguration, Long> {

    ApplicationConfiguration findByAlias(String alias);

}
