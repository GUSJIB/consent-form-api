package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.TestTemplates;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TestTemplatesRepository extends JpaRepository<TestTemplates, Long> {

    TestTemplates findById(Long id);

    TestTemplates findByName(String name);

    List<TestTemplates> findByCategory(Category category);

    Page<TestTemplates> findAll(Pageable pageable);

}
