package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Language;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.model.TemplateDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TemplateDetailRepository extends JpaRepository<TemplateDetail, Long> {

    List<TemplateDetail> findByTemplate(Template template);

    Template findByTemplateAndLanguage(Template template, Language language);
}
