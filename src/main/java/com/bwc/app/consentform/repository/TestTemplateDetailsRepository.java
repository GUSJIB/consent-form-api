package com.bwc.app.consentform.repository;

import com.bwc.app.consentform.model.Language;
import com.bwc.app.consentform.model.TestTemplates;
import com.bwc.app.consentform.model.TestTemplateDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TestTemplateDetailsRepository extends JpaRepository<TestTemplateDetails, Long> {

    TestTemplateDetails findById(Long id);

    Page<TestTemplateDetails> findAll(Pageable pageable);

    TestTemplateDetails findByTesttemplatesAndLanguage(TestTemplates testtemplates, Language language);

    List<TestTemplateDetails> findByTesttemplates(TestTemplates testtemplates);
}
