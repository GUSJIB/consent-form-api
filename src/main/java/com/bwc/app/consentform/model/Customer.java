package com.bwc.app.consentform.model;

import com.bwc.app.consentform.HIS.model.PatientInfo;
import com.bwc.app.consentform.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.FilterJoinTable;
import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "customers")
public class Customer implements Serializable {

    public Customer() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private Long id;

    @Column(name = "hn")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String hn;

    @Column(name = "firstname_th")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String firstnameTh;

    @Column(name = "middlename_th")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String middlenameTh;

    @Column(name = "lastname_th")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String lastnameTh;

    @Column(name = "firstname_en")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String firstnameEn;

    @Column(name = "middlename_en")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String middlenameEn;

    @Column(name = "lastname_en")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String lastnameEn;

    @Column(name = "title_th")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String titleTh;

    @Column(name = "title_en")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String titleEn;

    @Column(name = "papmi_row_id")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private Long papmiRowId;

    @Column(name = "dob")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private Date dob;

    @Column(name = "sex")
    @JsonView(Views.Single.class)
    private String sex;

    @Column(name = "nationality")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String nationality;

    @Column(name = "language_code")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String languageCode;

    @Column(name = "email")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private String email;

    @Column(name = "mobile_phone")
    @JsonView(Views.Single.class)
    private String mobilePhone;

    @Column(name = "home_phone")
    @JsonView(Views.Single.class)
    private String homePhone;

    @Column(name = "office_phone")
    @JsonView(Views.Single.class)
    private String officePhone;

    @Column(name = "address_no")
    @JsonView(Views.Single.class)
    private String addressNo;

    @Column(name = "area")
    @JsonView(Views.Single.class)
    private String area;

    @Column(name = "city")
    @JsonView(Views.Single.class)
    private String city;

    @Column(name = "province")
    @JsonView(Views.Single.class)
    private String province;

    @Column(name = "zipcode")
    @JsonView(Views.Single.class)
    private String zipcode;

    @Column(name = "vip")
    @JsonView(Views.Single.class)
    private Boolean vip;

    @Column(name = "vip_desc")
    @JsonView(Views.Single.class)
    private String vipDesc;

    @Column(name = "membership")
    @JsonView(Views.Single.class)
    private Boolean membership;

    @Column(name = "membership_at")
    @JsonView(Views.Single.class)
    private Date membershipAt;

    @Column(name = "photo")
    @JsonView(Views.Single.class)
    private String photo;

    @Column(name = "age_year")
    @JsonView(Views.Single.class)
    private Long ageYear;

    @Column(name = "age_month")
    @JsonView(Views.Single.class)
    private Long ageMonth;

    @Column(name = "age_day")
    @JsonView(Views.Single.class)
    private Long ageDay;

    @Column(name = "id_card")
    @JsonView(Views.Single.class)
    private String idCard;

    @Column(name = "passport_number")
    @JsonView(Views.Single.class)
    private String passportNumber;

    @Column(name = "secretary_number")
    @JsonView(Views.Single.class)
    private String secretaryNumber;


    @JsonView(Views.Single.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
    private Set<CustomerAgree> documents = new HashSet<>(0);

    @JsonView(Views.Single.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
    private Set<Attachment> attachments = new HashSet<>(0);


    // Getter and Setter
    @JsonView({Views.Multiple.class, Views.Single.class})
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getHn() {
        return hn.substring(0,2) + "-" + hn.substring(2,4) + "-" + hn.substring(4);
    }

    public void setHn(String hn) {
        this.hn = hn.replaceAll("-", "");
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getMiddlenameTh() {
        return middlenameTh;
    }

    public void setMiddlenameTh(String middlenameTh) {
        this.middlenameTh = middlenameTh;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(String firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getMiddlenameEn() {
        return middlenameEn;
    }

    public void setMiddlenameEn(String middlenameEn) {
        this.middlenameEn = middlenameEn;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getTitleTh() {
        return titleTh;
    }

    public void setTitleTh(String titleTh) {
        this.titleTh = titleTh;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Long getPapmiRowId() {
        return papmiRowId;
    }

    public void setPapmiRowId(Long papmiRowId) {
        this.papmiRowId = papmiRowId;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @JsonView(Views.Single.class)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonView(Views.Single.class)
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @JsonView(Views.Single.class)
    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    @JsonView(Views.Single.class)
    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    @JsonView(Views.Single.class)
    public String getAddressNo() {
        return addressNo;
    }

    public void setAddressNo(String addressNo) {
        this.addressNo = addressNo;
    }

    @JsonView(Views.Single.class)
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @JsonView(Views.Single.class)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonView(Views.Single.class)
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @JsonView(Views.Single.class)
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @JsonView(Views.Single.class)
    public Boolean getVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    @JsonView(Views.Single.class)
    public String getVipDesc() {
        return vipDesc;
    }

    public void setVipDesc(String vipDesc) {
        this.vipDesc = vipDesc;
    }

    @JsonView(Views.Single.class)
    public Boolean getMembership() {
        return membership;
    }

    public void setMembership(Boolean membership) {
        this.membership = membership;
    }

    @JsonView(Views.Single.class)
    public Date getMembershipAt() {
        return membershipAt;
    }

    public void setMembershipAt(Date membershipAt) {
        this.membershipAt = membershipAt;
    }

    @JsonView(Views.Single.class)
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @JsonView(Views.Single.class)
    public Long getAgeYear() {
        return ageYear;
    }

    public void setAgeYear(Long ageYear) {
        this.ageYear = ageYear;
    }

    @JsonView(Views.Single.class)
    public Long getAgeMonth() {
        return ageMonth;
    }

    public void setAgeMonth(Long ageMonth) {
        this.ageMonth = ageMonth;
    }

    @JsonView(Views.Single.class)
    public Long getAgeDay() {
        return ageDay;
    }

    public void setAgeDay(Long ageDay) {
        this.ageDay = ageDay;
    }

    @JsonView(Views.Single.class)
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @JsonView(Views.Single.class)
    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    @JsonView(Views.Single.class)
    public String getSecretaryNumber() {
        return secretaryNumber;
    }

    public void setSecretaryNumber(String secretaryNumber) {
        this.secretaryNumber = secretaryNumber;
    }

    @JsonView(Views.Single.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
    public Set<CustomerAgree> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<CustomerAgree> documents) {
        this.documents = documents;
    }

    @JsonView(Views.Single.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public Customer(PatientInfo p) throws ParseException {
        this.hn = p.getHN().replaceAll("-", "");
        this.firstnameTh = p.getPAPMIName();
        this.middlenameTh = p.getPAPMIName8();
        this.lastnameTh = p.getPAPMIName2();
        this.firstnameEn = p.getPAPMIName5();
        this.middlenameEn = p.getPAPMIName6();
        this.lastnameEn = p.getPAPMIName7();
        this.titleTh = p.getTitleTH();
        this.titleEn = p.getTitleEN();
        this.papmiRowId = new Long(p.getPAPMIRowId());
        this.dob = new SimpleDateFormat("yyyy-MM-dd").parse(p.getDOB());
        this.sex = p.getSex();
        this.nationality = p.getNation();
        this.languageCode = p.getlangcode();
        this.email = p.getemail();
        this.mobilePhone = p.getMobPhone();
        this.homePhone = p.gettelH();
        this.officePhone = p.gettelO();
        this.addressNo = p.getStreetNameLine();
        this.area = p.getCityArea();
        this.city = p.getCity();
        this.province = p.getprovince();
        this.zipcode = p.getZipCode();
        this.vip = (null == p.getvipflag()) ? false : true;
        this.vipDesc = p.getvipflagdesc();
        this.membership = (null == p.getMembership()) ? false : true;
        this.membershipAt = (null != p.getMembershipDateFrom()) ? new SimpleDateFormat("yyyy-MM-dd").parse(p.getMembershipDateFrom()) : null;
//        this.photo = p.getPatientPhoto ();
        this.ageYear = new Long(p.getageY());
        this.ageMonth = new Long(p.getageM());
        this.ageDay = new Long(p.getageD());
        this.idCard = p.getPAPMIID();
        this.passportNumber = p.getMembershipDateFrom();
        this.secretaryNumber = p.getSecretaryNumber();
    }
}
