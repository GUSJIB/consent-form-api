package com.bwc.app.consentform.model;

import com.bwc.app.consentform.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "templates")
public class Template implements Serializable {

    public Template() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "template")
    private TemplateDetail details;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonView(Views.Single.class)
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "template")
    public TemplateDetail getDetails() {
        return details;
    }

    public void setDetails(TemplateDetail details) {
        this.details = details;
    }
}
