package com.bwc.app.consentform.model;

import com.bwc.app.consentform.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "template_details")
public class TemplateDetail implements Serializable {

    public TemplateDetail() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "template_id")
    private Template template;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "language_id")
    @OrderColumn(name = "id")
    private Language language;

    @Column(name = "title")
    private String title;

    @Column(name = "body")
    private String body;

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getLanguage() {
        return language.getAlias();
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonView(Views.Single.class)
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
