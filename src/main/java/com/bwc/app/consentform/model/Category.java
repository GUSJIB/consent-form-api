package com.bwc.app.consentform.model;

import com.bwc.app.consentform.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "categories")
public class Category implements Serializable {

    public Category() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonView({Views.Multiple.class, Views.Single.class})
    private Long id;

    @JsonView({Views.Multiple.class, Views.Single.class})
    @Column(name = "name")
    private String name;

    @JsonView({Views.Multiple.class, Views.Single.class})
    @Column(name = "alias")
    private String alias;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "categories_permissions", joinColumns = @JoinColumn(name = "category_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @JsonView(Views.Single.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "category")
    @OrderBy(value = "id asc")
    private Set<Template> templates = new HashSet<Template>(
            0);

    @JsonView(Views.Single.class)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "category")
    @OrderBy(value = "id asc")
    private Set<TestTemplates> testtemplates = new HashSet<TestTemplates>(
            0);

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "category")
    public Set<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(Set<Template> templates) {
        this.templates = templates;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "category")
    public Set<TestTemplates> getTestTemplates() {
        return testtemplates;
    }

    public void setTestTemplates(Set<TestTemplates> testtemplates) {
        this.testtemplates = testtemplates;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "categories_permissions", joinColumns = @JoinColumn(name = "category_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    public Set<Role> getRoles() {
        return roles;
    }
}
