package com.bwc.app.consentform.model;

import com.bwc.app.consentform.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "attachments")
public class Attachment implements Serializable {

    public Attachment() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "expiry_at")
    private Date expiryAt;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "remark")
    private String remark;

    @Column(name = "documentId")
    private Long documentId;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Date getExpiryAt() {
        return expiryAt;
    }

    public void setExpiryAt(Date expiryAt) {
        this.expiryAt = expiryAt;
    }

    public void setFileName(String file) {
        this.fileName = file;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getFileName() {
        return fileName;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Long getDocumetId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    @JsonView({Views.Multiple.class, Views.Single.class})
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
