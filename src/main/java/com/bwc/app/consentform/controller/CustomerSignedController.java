package com.bwc.app.consentform.controller;

import com.bwc.app.consentform.model.CustomerAgree;
import com.bwc.app.consentform.service.CategoryService;
import com.bwc.app.consentform.service.CustomerAgreeService;
import com.bwc.app.consentform.service.CustomerService;
import com.bwc.app.consentform.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(produces = "application/json")
public class CustomerSignedController {

    private final CustomerService customerService;
    private final TemplateService templateService;
    private final CategoryService categoryService;
    private final CustomerAgreeService customerAgreeService;

    @Autowired
    CustomerSignedController(CustomerService customerService, TemplateService templateService, CategoryService categoryService, CustomerAgreeService customerAgreeService) {
        this.customerService = customerService;
        this.templateService = templateService;
        this.categoryService = categoryService;
        this.customerAgreeService = customerAgreeService;
    }

    @PostMapping(value = "/signed/{customerId}/{templateId}")
    public ResponseEntity<CustomerAgree> signed(@PathVariable("customerId") Long customerId, @PathVariable("templateId") Long templateId, @RequestBody CustomerAgree document) {
        document.setCustomer(customerService.findById(customerId));
        document.setTemplate(templateService.findById(templateId));
        return new ResponseEntity<CustomerAgree>(customerAgreeService.save(document), HttpStatus.OK);
    }


}
