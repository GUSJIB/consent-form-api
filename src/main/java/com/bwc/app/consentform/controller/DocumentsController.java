package com.bwc.app.consentform.controller;

import java.util.concurrent.TimeUnit;

import com.bwc.app.consentform.Views;
import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.model.TestTemplates;
import com.bwc.app.consentform.service.CategoryService;
import com.bwc.app.consentform.service.RoleService;
import com.bwc.app.consentform.service.TemplateService;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(produces = "application/json")
public class DocumentsController {

    private final TemplateService templateService;
    private final CategoryService categoryService;
    private final RoleService roleService;

    @Autowired
    DocumentsController(TemplateService templateService, CategoryService categoryService, RoleService roleService) {
        this.templateService = templateService;
        this.categoryService = categoryService;
        this.roleService = roleService;
    }

    @JsonView(Views.Multiple.class)
    @GetMapping(value = "/documents")
    public ResponseEntity<Page<Category>> getDocs(Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getPrincipal().toString();
        String role = auth.getAuthorities().stream().findFirst().get().getAuthority().toLowerCase();

        Page<Category> results = categoryService.findAllByRolesIn(roleService.findByAlias(role), pageable);
        return new ResponseEntity<Page<Category>>(results, HttpStatus.OK);
    }

    @GetMapping(value = "/document/{id}")
    public ResponseEntity<Template> getDocById(@PathVariable Long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getPrincipal().toString();
        String role = auth.getAuthorities().stream().findFirst().get().getAuthority().toLowerCase();

        Template template = templateService.findById(id);
        if(!template.getCategory().getRoles().contains(roleService.findByAlias(role))) {
            return new ResponseEntity<Template>(new Template(), HttpStatus.LOCKED);
        }
        return new ResponseEntity<Template>(template, HttpStatus.OK);
    }

    @JsonView(Views.Multiple.class)
    @GetMapping(value = "/testdocuments")
    public ResponseEntity<Page<TestTemplates>> getTestDocs(@PageableDefault(page = 0, size = 100) Pageable pageable) {
        Page<TestTemplates> results = templateService.findAllTest(pageable);
        return new ResponseEntity<Page<TestTemplates>>(results, HttpStatus.OK);
    }

    @GetMapping(value = "/testdocument/{id}")
    public ResponseEntity<TestTemplates> getTestDocById(@PathVariable Long id) {
        TestTemplates template = templateService.findTestById(id);
        return new ResponseEntity<TestTemplates>(template, HttpStatus.OK);
    }
}
