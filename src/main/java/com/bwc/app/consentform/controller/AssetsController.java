package com.bwc.app.consentform.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.ServletContextResource;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
@Controller
public class AssetsController {

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/assets/images/{**}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getImageAsByteArray(HttpServletRequest request) throws IOException {
        String path = request.getRequestURI();
        final InputStream in = servletContext.getResourceAsStream("/assets/images/" + path);
        return IOUtils.toByteArray(in);
    }

    @RequestMapping(value = "/assets/image/{path}/{file:.*}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getImageAsByteArray(@PathVariable String path, @PathVariable String file) throws IOException {
        final InputStream in = servletContext.getResourceAsStream("/assets/images/" + path + "/" + file);
        return IOUtils.toByteArray(in);
    }

    @RequestMapping(value = "/image/{file:.*}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getImageAsByteArray(@PathVariable String file) throws IOException {
        final InputStream in = servletContext.getResourceAsStream("/assets/images/" + file);
        return IOUtils.toByteArray(in);
    }

    @RequestMapping(value = "/assets/doctors/{file:.*}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getDoctorImageAsByteArray(@PathVariable String file) throws IOException {
        String path = environment.getProperty("doctor_uploaded_folder") + file;
        path = path.replace("\\", "/");
        final InputStream in = new FileInputStream(path);
        return IOUtils.toByteArray(in);
    }

    @RequestMapping(value = "/assets/images/groups/{file:.*}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getPackageImageAsByteArray(@PathVariable String file) throws IOException {
        String path = environment.getProperty("package_uploaded_folder") + file;
        path = path.replace("\\", "/");
        final InputStream in = new FileInputStream(path);
        return IOUtils.toByteArray(in);
    }

}