package com.bwc.app.consentform.controller;

import com.bwc.app.consentform.HIS.model.PatientInfo;
import com.bwc.app.consentform.HIS.model.SSUser;
import com.bwc.app.consentform.HIS.service.PatientInfoService;
import com.bwc.app.consentform.HIS.service.SSUserService;
import com.bwc.app.consentform.Views;
import com.bwc.app.consentform.model.Role;
import com.bwc.app.consentform.model.User;
import com.bwc.app.consentform.service.RoleService;
import com.bwc.app.consentform.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;

@RestController
@RequestMapping(produces = "application/json")
public class AuthController {

    private SSUserService ssUserService;
    private UserService userService;
    private RoleService roleService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private PatientInfoService patientInfoService;

    @Autowired
    AuthController(SSUserService ssUserService, UserService userService, RoleService roleService, BCryptPasswordEncoder bCryptPasswordEncoder, PatientInfoService patientInfoService) {
        this.ssUserService = ssUserService;
        this.userService = userService;
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.patientInfoService = patientInfoService;
    }

    @JsonView(Views.User.SignUp.class)
    @PostMapping("/users/sign-up")
    public ResponseEntity<User> signUp(@RequestBody User user) {

        User current_user = userService.findByUsername(user.getUsername());
        if(null == current_user) {
            SSUser ssUser = ssUserService.findByUsername(user.getUsername());
            if(ssUser == null){
                return ResponseEntity.notFound().build();
            }
            user.setName(ssUser.getName());
            user.setUsername(ssUser.getUsername());
            user.setRole(roleService.findByAlias("staff"));
            user.setActive(false);
            current_user = userService.save(user);
        }
        return new ResponseEntity<User>(current_user, HttpStatus.OK);
    }

    @JsonView(Views.User.SignUp.class)
    @PostMapping("/users/confirm-password")
    public ResponseEntity<User> confirmPassword(@RequestBody User user) {
        User current_user = userService.findByUsername(user.getUsername());
        if(null != current_user) {
            current_user.setActive(true);
            current_user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userService.save(current_user);
        }
        return new ResponseEntity<User>(current_user, HttpStatus.OK);
    }

    @GetMapping(value = "/photo/{hn}", produces = "image/jpeg")
    @ResponseBody
    public void getImageAsByteArray(@PathVariable("hn") String hn, HttpServletResponse response) throws IOException, SQLException {
        PatientInfo pt = patientInfoService.findByHn(hn);
        if(null != pt.getPatientPhoto()){
            InputStream inputStream = pt.getPatientPhoto().getBinaryStream();
            IOUtils.copy(inputStream, response.getOutputStream());
        }else {
            InputStream inputStream =  new URL("https://cdn1.iconfinder.com/data/icons/user-pictures/100/boy-512.png").openStream();
            IOUtils.copy(inputStream, response.getOutputStream());
        }
        response.flushBuffer();
    }

}
