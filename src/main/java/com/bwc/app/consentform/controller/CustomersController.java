package com.bwc.app.consentform.controller;

import com.bwc.app.consentform.Common.Helpers;
import com.bwc.app.consentform.HIS.model.PaAdm;
import com.bwc.app.consentform.HIS.model.PatientInfo;
import com.bwc.app.consentform.HIS.model.VisitSlip;
import com.bwc.app.consentform.HIS.service.PaAdmService;
import com.bwc.app.consentform.HIS.service.PatientInfoService;
import com.bwc.app.consentform.HIS.service.VisitSlipService;
import com.bwc.app.consentform.Views;
import com.bwc.app.consentform.model.Attachment;
import com.bwc.app.consentform.model.Customer;
import com.bwc.app.consentform.model.CustomerAgree;
import com.bwc.app.consentform.model.Role;
import com.bwc.app.consentform.service.AttachmentService;
import com.bwc.app.consentform.service.CustomerService;
import com.bwc.app.consentform.service.RoleService;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.rmi.CORBA.Util;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(produces = "application/json")
public class CustomersController {

    private ServletContext servletContext;
    private CustomerService customerService;
    private PatientInfoService patientInfoService;
    private RoleService roleService;
    private AttachmentService attachmentService;
    private PaAdmService paAdmService;
    private VisitSlipService visitSlipService;

    @Autowired
    CustomersController(ServletContext servletContext, CustomerService customerService, PatientInfoService patientInfoService, RoleService roleService, AttachmentService attachmentService, PaAdmService paAdmService, VisitSlipService visitSlipService) {
        this.servletContext = servletContext;
        this.customerService = customerService;
        this.patientInfoService = patientInfoService;
        this.roleService = roleService;
        this.attachmentService = attachmentService;
        this.paAdmService = paAdmService;
        this.visitSlipService = visitSlipService;
    }

    @JsonView(Views.Multiple.class)
    @GetMapping(value = "/customers")
    public ResponseEntity<Page<Customer>> index(@RequestParam(value = "q", defaultValue = "", required = false) String q, Pageable pageable) {
        if(q.isEmpty()) {
            return new ResponseEntity<Page<Customer>>(customerService.findAll(pageable), HttpStatus.OK);
        }else {
            return new ResponseEntity<Page<Customer>>(customerService.search(q, pageable), HttpStatus.OK);
        }
    }

    @JsonView(Views.Single.class)
    @GetMapping(value = "/customer/{hn}")
    public ResponseEntity<Customer> trakcareFindHn(@PathVariable String hn) throws ParseException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getPrincipal().toString();
        String roleAlias = auth.getAuthorities().stream().findFirst().get().getAuthority().toLowerCase();
        Role role = roleService.findByAlias(roleAlias);

        PatientInfo patient = patientInfoService.findByHn(hn);

        Customer customer = customerService.findByHn(hn);
        if(null != patient) {
            customer.setHn(patient.getHN().replaceAll("-", ""));
            customer.setFirstnameTh(patient.getPAPMIName());
            customer.setMiddlenameTh(patient.getPAPMIName8());
            customer.setLastnameTh(patient.getPAPMIName2());
            customer.setFirstnameEn(patient.getPAPMIName5());
            customer.setMiddlenameEn(patient.getPAPMIName6());
            customer.setLastnameEn(patient.getPAPMIName7());
            customer.setTitleTh(patient.getTitleTH());
            customer.setTitleEn(patient.getTitleEN());
            customer.setPapmiRowId(new Long(patient.getPAPMIRowId()));
            customer.setDob(new SimpleDateFormat("yyyy-MM-dd").parse(patient.getDOB()));
            customer.setSex(patient.getSex());
            customer.setNationality(patient.getNation());
            customer.setLanguageCode(patient.getlangcode());
            customer.setEmail(patient.getemail());
            customer.setMobilePhone(patient.getMobPhone());
            customer.setHomePhone(patient.gettelH());
            customer.setOfficePhone(patient.gettelO());
            customer.setAddressNo(patient.getStreetNameLine());
            customer.setArea(patient.getCityArea());
            customer.setCity(patient.getCity());
            customer.setProvince(patient.getprovince());
            customer.setZipcode(patient.getZipCode());
            customer.setVip((null == patient.getvipflag()) ? false : true);
            customer.setVipDesc(patient.getvipflagdesc());
            customer.setMembership((null == patient.getMembership()) ? false : true);
            customer.setMembershipAt((null != patient.getMembershipDateFrom()) ? new SimpleDateFormat("yyyy-MM-dd").parse(patient.getMembershipDateFrom()) : null);
            customer.setAgeYear(new Long(patient.getageY()));
            customer.setAgeMonth(new Long(patient.getageM()));
            customer.setAgeDay(new Long(patient.getageD()));
            customer.setIdCard(patient.getPAPMIID());
            customer.setPassportNumber(patient.getMembershipDateFrom());
            customer.setSecretaryNumber(patient.getSecretaryNumber());
            customer = customerService.save(customer);
        }

        Set<CustomerAgree> documents = new HashSet<>();
        customer.getDocuments().stream().forEach(doc -> {
            if(doc.getTemplate().getCategory().getRoles().contains(role)){
                documents.add(doc);
            }
        });
        customer.setDocuments(documents);
        return new ResponseEntity<Customer>(customer, HttpStatus.OK);
    }

    @GetMapping(value = "/customer/{hn}/today")
    public ResponseEntity<VisitSlip> getVisitSlipToday(@PathVariable String hn) {

        VisitSlip visitSlip = new VisitSlip();

        PaAdm paadm = paAdmService.findByHnToday(hn);
        if(paadm == null) {
            return new ResponseEntity<VisitSlip>(visitSlip, HttpStatus.NOT_FOUND);
        }
        visitSlip = visitSlipService.findByEpiRowId(paadm.getId());

        return new ResponseEntity<VisitSlip>(visitSlip, HttpStatus.OK);
    }

    @PostMapping(value = "/customer/{customerId}/attachment")
    public ResponseEntity<Attachment> saveAttachment(@PathVariable("customerId") Long customerId, @RequestBody Attachment attachment) {
        attachment.setCustomer(customerService.findById(customerId));
        return new ResponseEntity<Attachment>(attachmentService.save(attachment), HttpStatus.OK);
    }
}
