package com.bwc.app.consentform.controller;

import com.bwc.app.consentform.model.Attachment;
import com.bwc.app.consentform.service.AttachmentService;
import com.bwc.app.consentform.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.MessageDigest;
import java.nio.channels.NetworkChannel;
import java.util.Base64;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


@Controller
public class FilesController {

    private AttachmentService attachmentService;
    private CustomerService customerService;
    private Environment environment;

    @Autowired
    FilesController(AttachmentService attachmentService, Environment environment, CustomerService customerService) {
        this.attachmentService = attachmentService;
        this.environment = environment;
        this.customerService = customerService;
    }

    @RequestMapping(value = "/files/upload", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public ResponseEntity<Attachment> upload(@RequestParam("file") MultipartFile inputFile) {
        int width = 992;
        int height = 992;
        Attachment fileInfo = new Attachment();
        HttpHeaders headers = new HttpHeaders();
        if (!inputFile.isEmpty()) {
            try {
                String path = environment.getProperty("attachments_ uploaded_folder");
                String originalFilename = System.currentTimeMillis() + "." + inputFile.getOriginalFilename().split("\\.")[1];
                File destinationFile = new File( path + originalFilename);
                inputFile.transferTo(destinationFile);

                //resize
                int imageType = BufferedImage.TYPE_INT_RGB;
                BufferedImage scaledBI = new BufferedImage(width, height, imageType);
                Graphics2D g = scaledBI.createGraphics();
                g.setComposite(AlphaComposite.Src);
                g.drawImage(ImageIO.read(destinationFile), 0, 0, width, height, null);
                g.dispose();

                ImageIO.write(scaledBI, "PNG", destinationFile);

                fileInfo.setFileName(originalFilename);
                return new ResponseEntity<Attachment>(fileInfo, headers, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<Attachment>(HttpStatus.BAD_REQUEST);
            }
        }else{
            return new ResponseEntity<Attachment>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/files/watermarkimage", method = RequestMethod.POST)
    public ResponseEntity<Attachment> uploadAttachmentBase64(@RequestBody Attachment attachment) throws IOException {
        String path = environment.getProperty("attachments_uploaded_folder");

        //String fileName = System.currentTimeMillis() + ".png";
        String fileName = attachment.getRemark();

        String[] parts = attachment.getFileName().split(",");
        String imageString = parts[1];

        // create a buffered image
        BufferedImage image = null;
        byte[] imageByte;

        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        bis.close();

        // write the image to a file
        File outputfile = new File(path + fileName);
        ImageIO.write(image, "png", outputfile);

        Attachment att =  new Attachment();
        att.setFileName(fileName);
        return new ResponseEntity<Attachment>(att, HttpStatus.OK);
    }

    @RequestMapping(value = "/files/signature", method = RequestMethod.POST)
    public ResponseEntity<Attachment> uploadImageWithSignature(@RequestBody Attachment attachment) throws IOException {
        String path = environment.getProperty("attachments_uploaded_folder");

        String fileName = System.currentTimeMillis() + ".png";

        String[] parts = attachment.getFileName().split(",");
        String imageString = parts[1];

        // create a buffered image
        BufferedImage image = null;
        byte[] imageByte;

        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        bis.close();

        // write the image to a file
        File outputfile = new File(path + fileName);
        ImageIO.write(image, "png", outputfile);

        // Merge images
        // load source images
        BufferedImage imageBg = ImageIO.read(new File(path, attachment.getRemark()));
        BufferedImage overlay = ImageIO.read(new File(path, fileName));

        // create the new image, canvas size is the max. of both image sizes
        int w = Math.max(imageBg.getWidth(), overlay.getWidth());
        int h = Math.max(imageBg.getHeight(), overlay.getHeight());
        BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        // paint both images, preserving the alpha channels
        Graphics2D g = combined.createGraphics();
        g.drawImage(imageBg, 0, 0, null);
        g.drawImage(overlay, 0, 0, null);
        g.dispose();

        // Save as new image
        String fileNameCombined = System.currentTimeMillis() + ".png";
        ImageIO.write(combined, "PNG", new File(path, fileNameCombined));

        File file1 = new File(path + attachment.getRemark());
        file1.delete();
        File file2 = new File(path + fileName);
        file2.delete();

        Attachment att =  new Attachment();
        att.setFileName(fileNameCombined);
        return new ResponseEntity<Attachment>(att, HttpStatus.OK);
    }

    @GetMapping(value = "/files/{customerId}/{attachmentId}")
    public ResponseEntity<InputStreamResource> video(@PathVariable("customerId") Long customerId, @PathVariable("attachmentId") Long attachmentId, HttpServletRequest request)
            throws UnsupportedEncodingException, FileNotFoundException {
        Attachment attachment = attachmentService.findByCustomerAndId(customerService.findById(customerId), attachmentId);
        File read = new File(environment.getProperty("attachments_uploaded_folder") + attachment.getFileName());
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-disposition", "attachment; filename=\"" + read.getName() + "\"");
        return ResponseEntity.ok().headers(headers).contentLength(read.length())
                .contentType(MediaType.parseMediaType("image/png"))
                .body(new InputStreamResource(new FileInputStream(read)));
    }

    
    @GetMapping(value = "/files/test/{customerId}/{attachmentId}")
    public ResponseEntity<Attachment> getPicture(@PathVariable("customerId") Long customerId, @PathVariable("attachmentId") Long attachmentId, HttpServletRequest request)
            throws UnsupportedEncodingException, FileNotFoundException {
        Attachment attachment = attachmentService.findByCustomerAndId(customerService.findById(customerId), attachmentId);
        File read = new File(environment.getProperty("attachments_uploaded_folder") + attachment.getFileName());
        try (FileInputStream imageInFile = new FileInputStream(read)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int) read.length()];
            imageInFile.read(fileData);
            attachment.setFileName(Base64.getEncoder().encodeToString(fileData));
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return new ResponseEntity<Attachment>(attachment, HttpStatus.OK);
    }

    @GetMapping(value = "/files/attachments/{documentId}")
    public ResponseEntity<List<Attachment>> getAttachments(@PathVariable("documentId") Long documentId, HttpServletRequest request)
            throws UnsupportedEncodingException, FileNotFoundException {
        try
        {
        List<Attachment> attachment = attachmentService.findBydocumentId(documentId);
        return new ResponseEntity<List<Attachment>>(attachment, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    @PostMapping(value = "/files/emails")
    public void TestEmail() {
        // Recipient's email ID needs to be mentioned.
      String to = "Innocent1150@gmail.com";

      // Sender's email ID needs to be mentioned
      String from = "Kantinun@lansingbs.com";

      final String username = "Kantinun@lansingbs.com";//change accordingly
      final String password = "029322114";//change accordingly

      // Assuming you are sending email through relay.jangosmtp.net
      String host = "smtp.gmail.com";

      Properties props = new Properties();
      props.put("mail.smtp.auth", "true");
      //props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", host);
      props.put("mail.smtp.port", "465");
      props.put("mail.store.protocol", "pop3");
      props.put("mail.transport.protocol", "smtp");
      final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
      props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
      props.setProperty("mail.smtp.socketFactory.fallback", "false");

      // Get the Session object.
      Session session = Session.getInstance(props,
         new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(username, password);
            }
         });

      try {
         // Create a default MimeMessage object.
         Message message = new MimeMessage(session);    

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.setRecipients(Message.RecipientType.TO,
            InternetAddress.parse(to));

         // Set Subject: header field
         message.setSubject("Testing Subject");

         // Create the message part
         BodyPart messageBodyPart = new MimeBodyPart();

         // Now set the actual message
         messageBodyPart.setText("This is message body");

         // Create a multipar message
         Multipart multipart = new MimeMultipart();

         // Set text message part
         multipart.addBodyPart(messageBodyPart);

        //  // Part two is attachment
        //  messageBodyPart = new MimeBodyPart();
        //  String filename = "/home/manisha/file.txt";
        //  DataSource source = new FileDataSource(filename);
        //  messageBodyPart.setDataHandler(new DataHandler(source));
        //  messageBodyPart.setFileName(filename);
        //  multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart);

         // Send message
         Transport.send(message);

         System.out.println("Sent message successfully....");
  
      } catch (MessagingException e) {
         throw new RuntimeException(e);
      }
    }

    @PostMapping(value = "/files/hash")
    public ResponseEntity<String> TestHash()
    {
        try(InputStream in = new FileInputStream("C:/Users/Kantinun/Downloads/Rajinibon Report 2019.xlsx")) {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] block = new byte[4096];
            int length;
            while ((length = in.read(block)) > 0) {
                digest.update(block, 0, length);
            }
            return new ResponseEntity<String>(Base64.getEncoder().encodeToString(digest.digest()), HttpStatus.OK);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }
}
