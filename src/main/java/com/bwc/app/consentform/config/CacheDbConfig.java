package com.bwc.app.consentform.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "cacheEntityManagerFactory",
        transactionManagerRef = "cacheTransactionManager",
        basePackages = { "com.bwc.app.consentform.HIS.repo" }
)
public class CacheDbConfig {

    @Bean(name = "cacheDataSource")
    @ConfigurationProperties(prefix = "cache.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "cacheEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    cacheEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("cacheDataSource") DataSource dataSource
    ) {
        return
                builder
                        .dataSource(dataSource)
                        .packages("com.bwc.app.consentform.HIS.model")
                        .persistenceUnit("cache")
                        .build();
    }
    @Bean(name = "cacheTransactionManager")
    public PlatformTransactionManager cacheTransactionManager(
            @Qualifier("cacheEntityManagerFactory") EntityManagerFactory
                    cacheEntityManagerFactory
    ) {
        return new JpaTransactionManager(cacheEntityManagerFactory);
    }

}
