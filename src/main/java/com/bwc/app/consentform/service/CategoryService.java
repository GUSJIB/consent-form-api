package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CategoryService {

    Category findById(Long id);

    Category findByAlias(String alias);

    Page<Category> findAll(Pageable pageable);

    Page<Category> findAllByRolesIn(Role role, Pageable pageable);

}
