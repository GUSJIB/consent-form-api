package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Attachment;
import com.bwc.app.consentform.model.Customer;

import java.util.List;

public interface AttachmentService {

    Attachment findByFileName(String fileName);

    Attachment save(Attachment attachment);

    Attachment findById(Long id);

    Attachment findByCustomerAndId(Customer customer, Long id);

    List<Attachment> findBydocumentId(Long documentId);
}
