package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Language;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.model.TemplateDetail;
import com.bwc.app.consentform.repository.TemplateDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TemplateDetailServiceImpl implements TemplateDetailService {

    @Autowired
    private TemplateDetailRepository templateDetailRepository;

    public List<TemplateDetail> findByTemplate(Template template) {
        return templateDetailRepository.findByTemplate(template);
    }

    public Template findByTemplatAndLanguage(Template template, Language language) {
        return templateDetailRepository.findByTemplateAndLanguage(template, language);
    }
}
