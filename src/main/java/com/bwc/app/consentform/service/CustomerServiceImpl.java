package com.bwc.app.consentform.service;

import com.bwc.app.consentform.HIS.model.PatientInfo;
import com.bwc.app.consentform.HIS.service.PatientInfoService;
import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Customer;
import com.bwc.app.consentform.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final PatientInfoService patientInfoService;

    @Autowired
    CustomerServiceImpl(CustomerRepository customerRepository, PatientInfoService patientInfoService) {
        this.customerRepository = customerRepository;
        this.patientInfoService = patientInfoService;
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer findById(Long id) {
        return customerRepository.findOne(id);
    }

    public Customer findByHn(String hn) throws ParseException {

        Customer customer = customerRepository.findByHn(hn.replaceAll("-", ""));
        if(null == customer) {
            PatientInfo patient = patientInfoService.findByHn(hn);
            if(null != patient) {
                customer = new Customer(patient);
                customer = this.save(customer);
            }
        }else {

        }

        return customer;
    }

    public Page<Customer> findAllByHn(String hn, Pageable pageable) {
        return customerRepository.findAllByHn(hn.replaceAll("-", ""), pageable);
    }

    public Page<Customer> findAll(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    public Page<Customer> search(String q, Pageable pageable) {
        if(q.startsWith("81")) {
            q = q.replaceAll("-","");
            try {
                this.findByHn(q);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return customerRepository.search(q, pageable);
    }

}
