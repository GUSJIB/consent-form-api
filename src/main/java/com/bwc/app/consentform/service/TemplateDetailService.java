package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Language;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.model.TemplateDetail;

import java.util.List;

public interface TemplateDetailService {

    List<TemplateDetail> findByTemplate(Template template);

    Template findByTemplatAndLanguage(Template template, Language language);
}
