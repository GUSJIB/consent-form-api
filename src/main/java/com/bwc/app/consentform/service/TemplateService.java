package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.model.TestTemplates;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TemplateService {

    TestTemplates findTestById(Long id);

    Template findById(Long id);

    Template findByName(String name);

    List<Template> findByCategory(Category category);

    List<TestTemplates> findTestByCategory(Category category);

    Page<Template> findAll(Pageable pageable);

    Page<TestTemplates> findAllTest(Pageable pageable);
}
