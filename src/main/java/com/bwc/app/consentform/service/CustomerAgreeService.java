package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Customer;
import com.bwc.app.consentform.model.CustomerAgree;
import com.bwc.app.consentform.model.Template;

import java.util.List;

public interface CustomerAgreeService {

    List<CustomerAgree> findByCustomer(Customer customer);

    CustomerAgree findById(Long id);

    CustomerAgree findByCustomerAndTemplate(Customer customer, Template template);

    CustomerAgree save(CustomerAgree agree);
}
