package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.ApplicationConfiguration;
import com.bwc.app.consentform.repository.ApplicationConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ApplicationConfigurationServiceImpl implements ApplicationConfigurationService {

    @Autowired
    private ApplicationConfigurationRepository applicationConfigurationRepository;

    public List<ApplicationConfiguration> findAll() {
        return applicationConfigurationRepository.findAll();
    }

    public ApplicationConfiguration findByAlias(String alias) {
        return applicationConfigurationRepository.findByAlias(alias);
    }

}
