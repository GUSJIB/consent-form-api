package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Attachment;
import com.bwc.app.consentform.model.Customer;
import com.bwc.app.consentform.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentRepository attachmentRepository;

    public Attachment findByFileName(String fileName) {
        return attachmentRepository.findByFileName(fileName);
    }

    public Attachment save(Attachment attachment) {
        return attachmentRepository.save(attachment);
    }

    public Attachment findById(Long id) {
        return attachmentRepository.findOne(id);
    }

    public Attachment findByCustomerAndId(Customer customer, Long id) {
        return attachmentRepository.findByCustomerAndId(customer, id);
    }

    public List<Attachment> findBydocumentId(Long documentId){
        return attachmentRepository.findBydocumentId(documentId);
    }
}
