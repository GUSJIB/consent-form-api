package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Role;
import com.bwc.app.consentform.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role findByAlias(String alias) {
        return roleRepository.findByAlias(alias);
    }

    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }
}
