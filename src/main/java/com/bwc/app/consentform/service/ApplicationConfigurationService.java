package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.ApplicationConfiguration;

import java.util.List;

public interface ApplicationConfigurationService {

    ApplicationConfiguration findByAlias(String alias);

    List<ApplicationConfiguration> findAll();

}
