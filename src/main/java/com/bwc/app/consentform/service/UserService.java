package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.User;

import java.util.List;

public interface UserService {

    User findByUsername(String username);

    User save(User user);

    List<User> findAll();

}
