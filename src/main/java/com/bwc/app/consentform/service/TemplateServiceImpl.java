package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.TestTemplates;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.repository.TemplateRepository;
import com.bwc.app.consentform.repository.TestTemplatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TemplateServiceImpl implements TemplateService {

    @Autowired
    private TemplateRepository templateRepository;
    @Autowired
    private TestTemplatesRepository testtemplatesRepository;

    public Template findById(Long id) {
        return templateRepository.findById(id);
    }

    public Template findByName(String name) {
        return templateRepository.findByName(name);
    }

    public List<Template> findByCategory(Category category) {
        return templateRepository.findByCategory(category);
    }

    public List<TestTemplates> findTestByCategory(Category category)
    {
        return testtemplatesRepository.findByCategory(category);
    }

    public Page<Template> findAll(Pageable pageable) {
        return templateRepository.findAll(pageable);
    }
    
    public TestTemplates findTestById(Long id) {
        return testtemplatesRepository.findById(id);
    }

    public Page<TestTemplates> findAllTest(Pageable pageable) {
        return testtemplatesRepository.findAll(pageable);       
    }
}
