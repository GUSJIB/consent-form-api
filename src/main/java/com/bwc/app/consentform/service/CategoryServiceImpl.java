package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Role;
import com.bwc.app.consentform.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category findById(Long id) {
        return categoryRepository.findById(id);
    }

    public Category findByAlias(String alias) {
        return categoryRepository.findByAlias(alias);
    }

    public Page<Category> findAll(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }

    public Page<Category> findAllByRolesIn(Role role, Pageable pageable) {
        return categoryRepository.findAllByRolesIn(role, pageable);
    }

}
