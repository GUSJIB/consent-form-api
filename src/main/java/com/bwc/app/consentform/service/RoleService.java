package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Role;

public interface RoleService {

    Role findByAlias(String alias);

    Role findByName(String name);
}
