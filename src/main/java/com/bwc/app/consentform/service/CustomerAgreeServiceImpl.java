package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Customer;
import com.bwc.app.consentform.model.CustomerAgree;
import com.bwc.app.consentform.model.Template;
import com.bwc.app.consentform.repository.CustomerAgreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CustomerAgreeServiceImpl implements CustomerAgreeService {

    @Autowired
    private CustomerAgreeRepository customerAgreeRepository;

    public List<CustomerAgree> findByCustomer(Customer customer){
        return customerAgreeRepository.findByCustomer(customer);
    }

    public CustomerAgree findById(Long id) {
        return customerAgreeRepository.findById(id);
    }

    public CustomerAgree findByCustomerAndTemplate(Customer customer, Template template) {
        return customerAgreeRepository.findByCustomerAndTemplate(customer, template);
    }

    public CustomerAgree save(CustomerAgree agree) {
        return customerAgreeRepository.save(agree);
    }
}
