package com.bwc.app.consentform.service;

import com.bwc.app.consentform.model.Category;
import com.bwc.app.consentform.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.text.ParseException;

public interface CustomerService {

    Customer save(Customer customer);

    Customer findById(Long id);

    Customer findByHn(String hn) throws ParseException;

    Page<Customer> findAllByHn(String hn, Pageable pageable);

    Page<Customer> findAll(Pageable pageable);

    Page<Customer> search(String q, Pageable pageable);
}
