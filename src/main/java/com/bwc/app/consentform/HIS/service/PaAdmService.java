package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.PaAdm;

public interface PaAdmService {

    PaAdm findByHnToday(String hn);

}
