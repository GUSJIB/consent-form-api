package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.SSUser;

public interface SSUserService {

    SSUser findByUsername(String username);

}
