package com.bwc.app.consentform.HIS.repo;

import com.bwc.app.consentform.HIS.model.PaAdm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PaAdmRepository extends JpaRepository<PaAdm, Long> {

    @Query(value = "select PAADM_RowID from PA_Adm left join PA_Patmas on PAADM_PAPMI_DR = PAPMI_RowId where PAADM_AdmDate = current_date and PAPMI_NO = ?1", nativeQuery = true)
    PaAdm findByHnToday(String hn);

}
