package com.bwc.app.consentform.HIS.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;

@NamedStoredProcedureQuery(
        name = "findPatientInfoByHn",
        procedureName = "Custom_BWC_Store.BWCPatientInfo_getData",
        parameters = {
                @StoredProcedureParameter(name = "hn", type = String.class, mode = ParameterMode.IN)
        },
        resultClasses = { PatientInfo.class })
@Entity
public class PatientInfo implements Serializable {

    public PatientInfo() {}

    private String HN;

    public void setHN(String HN) {
        this.HN = HN;
    }

    public String getHN() {
        return HN;
    }

    private String PatientName;

    public void setPatientName(String PatientName) {
        this.PatientName = PatientName;
    }

    public String getPatientName() {
        return PatientName;
    }

    @Id
    private String PAPMIRowId;

    public void setPAPMIRowId(String PAPMIRowId) {
        this.PAPMIRowId = PAPMIRowId;
    }

    public String getPAPMIRowId() {
        return PAPMIRowId;
    }

    private String DOB;

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getDOB() {
        return DOB;
    }

    private String ageY;

    public void setageY(String ageY) {
        this.ageY = ageY;
    }

    public String getageY() {
        return ageY;
    }

    private String ageM;

    public void setageM(String ageM) {
        this.ageM = ageM;
    }

    public String getageM() {
        return ageM;
    }

    private String ageD;

    public void setageD(String ageD) {
        this.ageD = ageD;
    }

    public String getageD() {
        return ageD;
    }

    private String Sex;

    public void setSex(String Sex) {
        this.Sex = Sex;
    }

    public String getSex() {
        return Sex;
    }

    private String PAPMIID;

    public void setPAPMIID(String PAPMIID) {
        this.PAPMIID = PAPMIID;
    }

    public String getPAPMIID() {
        return PAPMIID;
    }

    private String Nation;

    public void setNation(String Nation) {
        this.Nation = Nation;
    }

    public String getNation() {
        return Nation;
    }

    private String telH;

    public void settelH(String telH) {
        this.telH = telH;
    }

    public String gettelH() {
        return telH;
    }

    private String telO;

    public void settelO(String telO) {
        this.telO = telO;
    }

    public String gettelO() {
        return telO;
    }

    private String MobPhone;

    public void setMobPhone(String MobPhone) {
        this.MobPhone = MobPhone;
    }

    public String getMobPhone() {
        return MobPhone;
    }

    private String email;

    public void setemail(String email) {
        this.email = email;
    }

    public String getemail() {
        return email;
    }

    private String StreetNameLine;

    public void setStreetNameLine(String StreetNameLine) {
        this.StreetNameLine = StreetNameLine;
    }

    public String getStreetNameLine() {
        return StreetNameLine;
    }

    private String CityArea;

    public void setCityArea(String CityArea) {
        this.CityArea = CityArea;
    }

    public String getCityArea() {
        return CityArea;
    }

    private String City;

    public void setCity(String City) {
        this.City = City;
    }

    public String getCity() {
        return City;
    }

    private String province;

    public void setprovince(String province) {
        this.province = province;
    }

    public String getprovince() {
        return province;
    }

    private String ZipCode;

    public void setZipCode(String ZipCode) {
        this.ZipCode = ZipCode;
    }

    public String getZipCode() {
        return ZipCode;
    }

    private String PAPMIName;

    public void setPAPMIName(String PAPMIName) {
        this.PAPMIName = PAPMIName;
    }

    public String getPAPMIName() {
        return PAPMIName;
    }

    private String PAPMIName2;

    public void setPAPMIName2(String PAPMIName2) {
        this.PAPMIName2 = PAPMIName2;
    }

    public String getPAPMIName2() {
        return PAPMIName2;
    }

    private String PAPMIName3;

    public void setPAPMIName3(String PAPMIName3) {
        this.PAPMIName3 = PAPMIName3;
    }

    public String getPAPMIName3() {
        return PAPMIName3;
    }

    private Blob PatientPhoto;

    public void setPatientPhoto(Blob PatientPhoto) {
        this.PatientPhoto = PatientPhoto;
    }

    public Blob getPatientPhoto() {
        return PatientPhoto;
    }

    private String Membership;

    public void setMembership(String Membership) {
        this.Membership = Membership;
    }

    public String getMembership() {
        return Membership;
    }

    private String MembershipDateFrom;

    public void setMembershipDateFrom(String MembershipDateFrom) {
        this.MembershipDateFrom = MembershipDateFrom;
    }

    public String getMembershipDateFrom() {
        return MembershipDateFrom;
    }

    private String vipflag;

    public void setvipflag(String vipflag) {
        this.vipflag = vipflag;
    }

    public String getvipflag() {
        return vipflag;
    }

    private String vipflagdesc;

    public void setvipflagdesc(String vipflagdesc) {
        this.vipflagdesc = vipflagdesc;
    }

    public String getvipflagdesc() {
        return vipflagdesc;
    }

    private String langcode;

    public void setlangcode(String langcode) {
        this.langcode = langcode;
    }

    public String getlangcode() {
        return langcode;
    }

    private String PAPMIName5;

    public void setPAPMIName5(String PAPMIName5) {
        this.PAPMIName5 = PAPMIName5;
    }

    public String getPAPMIName5() {
        return PAPMIName5;
    }

    private String PAPMIName6;

    public void setPAPMIName6(String PAPMIName6) {
        this.PAPMIName6 = PAPMIName6;
    }

    public String getPAPMIName6() {
        return PAPMIName6;
    }

    private String PAPMIName7;

    public void setPAPMIName7(String PAPMIName7) {
        this.PAPMIName7 = PAPMIName7;
    }

    public String getPAPMIName7() {
        return PAPMIName7;
    }

    private String PAPMIName8;

    public void setPAPMIName8(String PAPMIName8) {
        this.PAPMIName8 = PAPMIName8;
    }

    public String getPAPMIName8() {
        return PAPMIName8;
    }

    private String TitleTH;

    public void setTitleTH(String TitleTH) {
        this.TitleTH = TitleTH;
    }

    public String getTitleTH() {
        return TitleTH;
    }

    private String TitleEN;

    public void setTitleEN(String TitleEN) {
        this.TitleEN = TitleEN;
    }

    public String getTitleEN() {
        return TitleEN;
    }

    private String PassportNumber;

    public String getPassportNumber() {
        return PassportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        PassportNumber = passportNumber;
    }

    private String SecretaryNumber;

    public String getSecretaryNumber() {
        return SecretaryNumber;
    }

    public void setSecretaryNumber(String secretaryNumber) {
        SecretaryNumber = secretaryNumber;
    }
}
