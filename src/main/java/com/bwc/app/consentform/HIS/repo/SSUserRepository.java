package com.bwc.app.consentform.HIS.repo;

import com.bwc.app.consentform.HIS.model.SSUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SSUserRepository extends JpaRepository<SSUser, Long> {

    SSUser findByUsername(String username);

}
