package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.PaAdm;
import com.bwc.app.consentform.HIS.repo.PaAdmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class PaAdmServiceImpl implements PaAdmService {

    @Autowired
    private PaAdmRepository paAdmRepository;

    public PaAdm findByHnToday(String hn) {
        return paAdmRepository.findByHnToday(hn);
    }
}
