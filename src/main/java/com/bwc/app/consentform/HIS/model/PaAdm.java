package com.bwc.app.consentform.HIS.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "SQLUser.PA_ADM")
@Entity
public class PaAdm implements Serializable {

    public PaAdm() {}

    @Id
    @Column(name = "PAADM_ROWID")
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
