package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.VisitSlip;

public interface VisitSlipService {

    VisitSlip findByEpiRowId(Long epiRowId);
}
