package com.bwc.app.consentform.HIS.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "SQLUser.SS_USER")
@Entity
public class SSUser implements Serializable {

    public SSUser() {}

    @Id
    @Column(name = "SSUSR_RowId")
    private Long id;

    @Column(name = "SSUSR_Initials")
    private String username;

    @Column(name = "SSUSR_Password")
    private String password;

    @Column(name = "SSUSR_Name")
    private String name;

    @Column(name = "SSUSR_PasswordSalt")
    private String salt;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
