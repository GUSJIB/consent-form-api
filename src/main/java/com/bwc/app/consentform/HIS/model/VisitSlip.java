package com.bwc.app.consentform.HIS.model;

import javax.persistence.*;
import java.io.Serializable;

@NamedStoredProcedureQuery(
        name = "findVisitSlipByEpiRowId",
        procedureName = "Custom_BWC_Store.BWCVisitSlip_getData",
        parameters = {
                @StoredProcedureParameter(name = "EpisodeRowid", type = Long.class, mode = ParameterMode.IN)
        },
        resultClasses = { VisitSlip.class })
@Entity
public class VisitSlip implements Serializable {

    public VisitSlip() {}

    @Id
    private String PAADMRowId;
    private String EpisodeNo;
    private String EpisodeDate;
    private String EpisodeTime;
    private String CareprovName;
    private String LocDesc;

    public String getCareprovName() {
        return CareprovName;
    }

    public String getEpisodeDate() {
        return EpisodeDate;
    }

    public String getEpisodeNo() {
        return EpisodeNo;
    }

    public String getEpisodeTime() {
        return EpisodeTime;
    }

    public String getLocDesc() {
        return LocDesc;
    }

    public String getPAADMRowId() {
        return PAADMRowId;
    }

    public void setEpisodeDate(String episodeDate) {
        EpisodeDate = episodeDate;
    }

    public void setCareprovName(String careprovName) {
        CareprovName = careprovName;
    }

    public void setEpisodeNo(String episodeNo) {
        EpisodeNo = episodeNo;
    }

    public void setEpisodeTime(String episodeTime) {
        EpisodeTime = episodeTime;
    }

    public void setLocDesc(String locDesc) {
        LocDesc = locDesc;
    }

    public void setPAADMRowId(String PAADMRowId) {
        this.PAADMRowId = PAADMRowId;
    }

}
