package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.PatientInfo;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PatientInfoServiceImpl implements PatientInfoService {

    @PersistenceContext(unitName="cache")
    private EntityManager manager;

    public PatientInfo findByHn(String hn) {

        StoredProcedureQuery storedProcedure = manager.createStoredProcedureQuery("Custom_BWC_Store.BWCPatientInfo_getData", PatientInfo.class)
                .registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
        storedProcedure.setParameter(0, hn).execute();

        List<PatientInfo> result = storedProcedure.getResultList();

        return result.stream().findFirst().get();
    }

}
