package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.PatientInfo;
import com.bwc.app.consentform.HIS.model.VisitSlip;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class VisitSlipServiceImpl implements VisitSlipService {

    @PersistenceContext(unitName="cache")
    private EntityManager manager;

    public VisitSlip findByEpiRowId(Long epiRowId) {
        StoredProcedureQuery storedProcedure = manager.createStoredProcedureQuery("Custom_BWC_Store.BWCVisitSlip_getData", VisitSlip.class)
                .registerStoredProcedureParameter(0, Long.class, ParameterMode.IN);
        storedProcedure.setParameter(0, epiRowId).execute();

        List<VisitSlip> result = storedProcedure.getResultList();

        return result.stream().findFirst().get();
    }
}
