package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.SSUser;
import com.bwc.app.consentform.HIS.repo.SSUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class SSUserServiceImpl implements SSUserService {

    @Autowired
    private SSUserRepository ssUserRepository;

    public SSUser findByUsername(String username) {
        return ssUserRepository.findByUsername(username);
    }
}
