package com.bwc.app.consentform.HIS.service;

import com.bwc.app.consentform.HIS.model.PatientInfo;

public interface PatientInfoService {

    PatientInfo findByHn(String hn);

}
