package com.bwc.app.consentform.security;

public class SecurityConstants {

    public static final String SECRET = "SecretKeyToGenJWTsForConsentForm";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String CONFIRM_PASSWORD_URL = "/users/confirm-password";

}
